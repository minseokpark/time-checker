package com.gachon.timechecker

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

/**
 * Created by minseok on 2019-09-15.
 * TimeChecker.
 */
class LiveDataTest {
    @get:Rule
    var rule = InstantTaskExecutorRule()

    val _liveDataName = MutableLiveData<String>()
    val _liveDataA = MutableLiveData<ModelA>()

    val INPUT_SAMPLE_MODEL_NAME = "name"

    @Test
    fun basic_map_test() {
        // Given..
        _liveDataA.value = ModelA(INPUT_SAMPLE_MODEL_NAME)

        // When..
        val newModel = Transformations.map(_liveDataA) { modelA ->
            modelA.toModelB()
        }

        // Then..
        assertEquals(newModel, ModelB(INPUT_SAMPLE_MODEL_NAME))
    }

    @Test
    fun basic_swtichmap_test() {
        // Given..
        _liveDataName.value = INPUT_SAMPLE_MODEL_NAME
        _liveDataA.value = ModelA(INPUT_SAMPLE_MODEL_NAME)

        // When..
        val newModel = Transformations.switchMap(_liveDataName) { name ->
            getUserId(name)
        }

        // Then..
        assertEquals(newModel.value, ModelB(INPUT_SAMPLE_MODEL_NAME))
    }

    fun getUserId(name: String): LiveData<ModelB> {
        return MutableLiveData<ModelB>().apply {
            this.value = ModelB(name)
        }
    }

    data class ModelA(val name:String) {
        fun toModelB() = ModelB(this.name)
    }
    data class ModelB(val name:String)
}