package com.gachon.timechecker

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun `joinToString`() {
        // Given..
        val answer = "a, b, c"

        // When..
        val result= listOf("a", "b", "c").joinToString()

        // Then..
        assertEquals(answer, result)
    }

    @Test
    fun `dfadsfsd`() {
        val challengeTime = 1572868519669

        // 1
//         val targetTime = 1572928909669

        // 2
//         val targetTime = 1572928999669

        // 3
//         val targetTime = 1573049759669

        // 4
//         val targetTime = 1573110039669

        // 5
//         val targetTime = 1573110459669

        val n = classifyWeek(challengeTime, targetTime)
        println(n)
        assertEquals(1, n)

//        1: 1572928999669
//        2: 1572989479669
//        3: 1573049959669
//        4: 1573110439669
    }

    private fun classifyWeek(startTime: Long, time: Long): Int {
        val firstCheckPoint = startTime + 60480000
        val secondCheckPoint = firstCheckPoint + 60480000
        val thirdCheckPoint = secondCheckPoint + 60480000
        val fourthCheckPoint = thirdCheckPoint + 60480000

        println("1: " + firstCheckPoint)
        println("2: " + secondCheckPoint)
        println("3: " + thirdCheckPoint)
        println("4: " + fourthCheckPoint)

        return when {

            time < firstCheckPoint -> 0
            time < secondCheckPoint -> 1
            time < thirdCheckPoint -> 2
            time < fourthCheckPoint -> 3
            else -> 4
        }
    }
}
