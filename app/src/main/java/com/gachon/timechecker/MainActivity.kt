package com.gachon.timechecker

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.gachon.timechecker.challenge.ChallengeFragment
import com.gachon.timechecker.mypage.MypageFragment
import com.gachon.timechecker.posts.PostFragment
import com.gachon.timechecker.selectchallenge.SelectChallengeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        changePage(SelectChallengeFragment.newInstance())
        navigation_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_main -> SelectChallengeFragment.newInstance()
                R.id.navigation_camera -> ChallengeFragment.newInstance()
                R.id.navigation_posts -> PostFragment.newInstance()
                R.id.navigation_mypage -> MypageFragment.newInstance()
                else -> null
            }.also { page ->  changePage(page) }

            true
        }
    }

    private fun changePage(fragment: Fragment?) {
        if (fragment == null) return

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container_fragment, fragment)
            commitAllowingStateLoss()
        }
    }
}
