package com.gachon.timechecker.common

import android.app.Application
import com.gachon.timechecker.repository.UserSetting

/**
 * Created by minseok on 2019-09-23.
 * TimeChecker.
 */
class TimeCheckerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        UserSetting.init(this)
    }
}