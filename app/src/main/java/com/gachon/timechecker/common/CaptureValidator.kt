package com.gachon.timechecker.common

import android.util.Log
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import java.util.*

/**
 * Created by minseok on 2019-10-27.
 * TimeChecker.
 */
object CaptureValidator {
    val TAG = "CaptureValidator"
    fun provideDetector(): FirebaseVisionTextRecognizer? {
        val options = FirebaseVisionCloudTextRecognizerOptions.Builder()
                .setLanguageHints(Arrays.asList("en", "hi"))
                .build()

        if (isCapture) {
            Log.d(TAG, "is Already Captrue")
            return null
        }

        Log.d(TAG, "is Not Capture")
//        return FirebaseVision.getInstance().getText(options)
        return FirebaseVision.getInstance().onDeviceTextRecognizer
    }

    var isCapture = false
}