package com.gachon.timechecker.common

import android.content.res.Resources
import android.net.Uri
import android.util.TypedValue
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImage(url: String) {
    if (url.contains("gs://")) {
        FileManager.toUri(url, object : FileManager.DownloadCallback {
            override fun onSuccess(uri: Uri) {
                Glide.with(context).load(uri).into(this@setImage)
            }
        })
    } else {
        Glide.with(context).load(url).into(this)
    }
}

fun pxTodp(size: Int): Int {
    val r = Resources.getSystem()
    val dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size.toFloat(), r.displayMetrics)

    return dp.toInt()
}