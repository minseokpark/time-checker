package com.gachon.timechecker.common

import com.gachon.timechecker.data.Challenge
import java.util.*

/**
 * Created by minseok on 2019-10-19.
 * TimeChecker.
 */
fun getElapsedTime(challengeToDo: Challenge): Pair<Int, Int> {
    val startRawTime = challengeToDo.startTime.split(":").map { it.toInt() }
    val endRawTime = challengeToDo.endTime.split(":").map { it.toInt() }
    val startTime = getTime(startRawTime[0], startRawTime[1])
    val endTime = getTime(endRawTime[0], endRawTime[1])
    val timeMillis = (endTime.timeInMillis - startTime.timeInMillis) / 1000

    val hour = timeMillis / 3600
    val min = (timeMillis % 3600) / 60

    return hour.toInt() to min.toInt()
}

fun getTime(hour: Int, min: Int): Calendar {
    return Calendar.getInstance().apply {
        this.set(Calendar.HOUR_OF_DAY, hour)
        this.set(Calendar.MINUTE, min)
    }
}