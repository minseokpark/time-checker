package com.gachon.timechecker.common

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.gachon.timechecker.R

/**
 * Created by minseok on 2019-10-27.
 * TimeChecker.
 */
open class ExtensionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setProgress()
    }

    private var progressDialog: AppCompatDialog? = null
    private val SHOWPROGRESS = 1000
    private val HIDEPROGRESS = 1001

    fun setProgress() {
        progressDialog = AppCompatDialog(this).apply {
            this.setCancelable(false)
        }
    }

    fun startProgress() {
        Message().apply {
            this.arg1 = SHOWPROGRESS
        }.also {
            progressHandler.sendMessage(it)
        }
    }

    fun endProgress() {
        Message().apply {
            this.arg1 = HIDEPROGRESS
        }.also {
            progressHandler.sendMessage(it)
        }
    }

    var progressHandler = Handler(object : Handler.Callback {
        override fun handleMessage(msg: Message): Boolean {
            val id = msg.arg1
            if (id == SHOWPROGRESS) {
                progressDialog?.window?.setBackgroundDrawable(ColorDrawable((Color.TRANSPARENT)))
                progressDialog?.setContentView(R.layout.progress)
                progressDialog?.show()
            } else if (id == HIDEPROGRESS) {
                if (progressDialog != null && progressDialog?.isShowing == true) {
                    progressDialog?.dismiss()
                }
            }


            return false
        }
    })
}