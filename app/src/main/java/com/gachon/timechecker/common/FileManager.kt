package com.gachon.timechecker.common

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import com.gachon.timechecker.GlobalDef
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageException
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by minseok on 2019-09-29.
 * TimeChecker.
 */
class FileManager {

    companion object {
        private var INSTANCE: FirebaseStorage? = null

        fun uploadFile(fileName: String, file: File, callback: UploadCallback) {
            val storage = initConnect()

            // Create a storage reference from our app
            val storageRef = storage.reference

            var path = "/images/${fileName}"
            val file = Uri.fromFile(file)
            val riversRef = storageRef.child(path)
            val uploadTask = riversRef.putFile(file)

            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                val e = it as StorageException
                // Handle unsuccessful uploads
                log(it.toString())
                log(it.message ?: "")
                log(e.errorCode.toString() ?: "")
                log(e.httpResultCode.toString() ?: "")
                log("이미지 로드 실패")

                callback.onFail()
            }.addOnSuccessListener {
                log("이미지 로드 성공")

                val imageUrl = GlobalDef.URL_FIRESTORE_STORAGE + path
                callback.onSuccess(imageUrl)
            }
        }

        fun toUri(url: String, callback: DownloadCallback) {
            if (url.isEmpty()) return

            val ref = initConnect()
            val gsReference = ref.getReferenceFromUrl(url)
            gsReference.downloadUrl.addOnSuccessListener {
                callback.onSuccess(it)
            }
        }

        private fun createFile(baseFolder: File, format: String, extension: String) =
                File(baseFolder, SimpleDateFormat(format, Locale.US)
                        .format(System.currentTimeMillis()) + extension)

        private fun initConnect(): FirebaseStorage {
            if (INSTANCE == null) {
                INSTANCE = FirebaseStorage.getInstance(GlobalDef.URL_FIRESTORE_STORAGE)
            }

            return INSTANCE!!
        }

        private fun log(log: String) {
            Log.d("TAG", log)
        }

        fun saveBitmapToFileCache(bitmap: Bitmap, filePath: String, fileName: String, success: ()->Unit) {
            val file = File(filePath)
            if (!file.exists()) {
                file.mkdirs()
            }

            val fileCache = File(filePath + fileName)
            var out: OutputStream? = null
            try {
                fileCache.createNewFile()
                out = FileOutputStream(fileCache)

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    out?.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                success()
            }
        }
    }

    interface UploadCallback {
        fun onSuccess(url: String)
        fun onFail()
    }

    interface DownloadCallback {
        fun onSuccess(uri: Uri)
    }
}