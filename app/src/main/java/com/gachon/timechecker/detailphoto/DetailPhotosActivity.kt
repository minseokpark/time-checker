package com.gachon.timechecker.detailphoto

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gachon.timechecker.R
import com.gachon.timechecker.statistic.PhotosAdapter
import kotlinx.android.synthetic.main.activity_detail_photos.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DetailPhotosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photos)

        val title = intent.getStringExtra("title")
        tv_title.text = title
    }

    override fun onStart() {
        super.onStart()
        val photos = intent.getStringArrayListExtra("photos").toList()

        CoroutineScope(Dispatchers.IO).launch {
            delay(300)
            CoroutineScope(Dispatchers.Main).launch {
                val adapter = PhotosAdapter(this@DetailPhotosActivity, photos, container_photos.columnWidth)
                container_photos.adapter = adapter
            }
        }
    }
}
