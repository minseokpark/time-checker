package com.gachon.timechecker.detailphoto

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.gachon.timechecker.R
import com.gachon.timechecker.common.FileManager
import kotlinx.android.synthetic.main.activity_detail_photo.*
import kotlinx.android.synthetic.main.item_photo.*

class DetailPhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photo)

        btn_close.setOnClickListener { finish() }

        val url = intent?.getStringExtra("url") ?: ""
        if (url.isEmpty()) {
            finish()
        }

        if (url.contains("gs://")) {
            FileManager.toUri(url, object : FileManager.DownloadCallback {
                override fun onSuccess(uri: Uri) {
                    Glide.with(this@DetailPhotoActivity).load(uri).into(iv_image)
                }
            })
        } else {
            Glide.with(this).load(url).into(iv)
        }
    }
}
