package com.gachon.timechecker.photo

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import com.gachon.timechecker.R
import kotlinx.android.synthetic.main.activity_photo_capture.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class PhotoCaptureActivity : AppCompatActivity() {
    var bitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_capture)

        startClockRealTime()

        bitmap = BitmapFactory.decodeResource(resources, R.drawable.sample)
                .copy(Bitmap.Config.ARGB_8888, true)

        btn_pic.setOnClickListener {
            val permissions = arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 0)
            }

            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also {
                startActivityForResult(it, 1)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != 0) {
            if (data != null) {
                val bm = data.extras.get("data") as Bitmap
                bitmap = bm.copy(Bitmap.Config.ARGB_8888, true)
                iv_image.setImageBitmap(bitmap)
            }
        }
    }

    private fun startClockRealTime() {
        CoroutineScope(Dispatchers.IO).launch {
            while (true) {
                val time = Date()
                val timeString = SimpleDateFormat("yyyy년 MM월 dd일 a hh시 mm분 ss초", Locale.KOREA).format(time)
                updateUI(timeString)
                delay(1000)
            }
        }
    }

    private fun updateUI(time: String) {
        CoroutineScope(Dispatchers.Main).launch {
            val drawable = writeTextOnDrawable(time)
            iv_image.setImageDrawable(drawable)
        }
    }

    private fun writeTextOnDrawable(text: String): BitmapDrawable {
        val typeFace = Typeface.create("Monospace", Typeface.BOLD)

        val paint = Paint().apply {
            this.style = Paint.Style.FILL
            this.color = Color.WHITE
            this.typeface = typeFace
            this.textAlign = Paint.Align.CENTER
            this.textSize = 30.toPx(this@PhotoCaptureActivity)
        }

        val textRect = Rect().apply {
            paint.getTextBounds(text, 0, text.length, this)
        }

        val canvas = Canvas(bitmap)
        if (textRect.width() >= canvas.width - 4) {
            paint.textSize = 7.toPx(this)
        }

        val xPos = ((canvas.width / 2) - 2).toFloat()
        val yPos = (canvas.height / 2) - ((paint.descent() + paint.ascent()) / 2)

        canvas.drawText(text, xPos, yPos, paint)

        return BitmapDrawable(resources, bitmap)
    }

    private fun Int.toPx(context: Context): Float {
        val conversionScale = context.getResources().getDisplayMetrics().density

        return (this * conversionScale + 0.5f)

    }
}
