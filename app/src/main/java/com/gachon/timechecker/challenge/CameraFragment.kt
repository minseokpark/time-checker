package com.gachon.timechecker.challenge

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.hardware.Camera
import android.hardware.display.DisplayManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.util.Rational
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.camera.core.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gachon.timechecker.R
import com.gachon.timechecker.common.AutoFitPreviewBuilder
import com.gachon.timechecker.common.CaptureValidator
import com.gachon.timechecker.common.FileCompress
import com.gachon.timechecker.common.FileManager
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.text.FirebaseVisionText
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.*

typealias LumaListener = (luma: ByteArray) -> Unit

const val KEY_EVENT_ACTION = "key_event_action"
const val KEY_EVENT_EXTRA = "key_event_extra"

class CameraFragment : Fragment() {

    private lateinit var container: ConstraintLayout
    private lateinit var viewFinder: TextureView
    private lateinit var outputDirectory: File
    private lateinit var broadcastManager: LocalBroadcastManager

    private var displayId = -1
    private var lensFacing = CameraX.LensFacing.BACK
    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var imageAnalyzer: ImageAnalysis? = null

    private lateinit var displayManager: DisplayManager

    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = view?.let { view ->
            if (displayId == this@CameraFragment.displayId) {
                Log.d(TAG, "Rotation changed: ${view.display.rotation}")
                preview?.setTargetRotation(view.display.rotation)
                imageCapture?.setTargetRotation(view.display.rotation)
                imageAnalyzer?.setTargetRotation(view.display.rotation)
            }
        } ?: Unit
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onDestroyView() {
        super.onDestroyView()

        displayManager.unregisterDisplayListener(displayListener)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_camera, container, false)

    /** Define callback that will be triggered after a photo has been taken and saved to disk */
    private val imageSavedListener = object : ImageCapture.OnImageSavedListener {
        override fun onError(
                error: ImageCapture.ImageCaptureError, message: String, exc: Throwable?) {
            Log.e(TAG, "Photo capture failed: $message")
            exc?.printStackTrace()
        }

        override fun onImageSaved(photoFile: File) {
            Log.d(TAG, "Photo capture succeeded: ${photoFile.absolutePath}")

            // Implicit broadcasts will be ignored for devices running API
            // level >= 24, so if you only target 24+ you can remove this statement
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                requireActivity().sendBroadcast(
                        Intent(Camera.ACTION_NEW_PICTURE, Uri.fromFile(photoFile)))
            }

            // If the folder selected is an external media directory, this is unnecessary
            // but otherwise other apps will not be able to access our images unless we
            // scan them using [MediaScannerConnection]
            val mimeType = MimeTypeMap.getSingleton()
                    .getMimeTypeFromExtension(photoFile.extension)
            MediaScannerConnection.scanFile(
                    context, arrayOf(photoFile.absolutePath), arrayOf(mimeType), null)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container = view as ConstraintLayout
        viewFinder = container.findViewById(R.id.view_finder)
        broadcastManager = LocalBroadcastManager.getInstance(view.context)

        // Every time the orientation of device changes, recompute layout
        displayManager = viewFinder.context
                .getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        displayManager.registerDisplayListener(displayListener, null)

        // Determine the output directory
        outputDirectory = getOutputDirectory(requireContext())

        // Wait for the views to be properly laid out
        viewFinder.post {
            // Keep track of the display in which this view is attached
            displayId = viewFinder.display.displayId

            // Build UI controls and bind all camera use cases
            updateCameraUi()
            bindCameraUseCases()
        }
    }

    /** Declare and bind preview, capture and analysis use cases */
    private fun bindCameraUseCases() {

        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }
        val screenAspectRatio = Rational(metrics.widthPixels, metrics.heightPixels)
        Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")

        // Set up the view finder use case to display camera preview
        val viewFinderConfig = PreviewConfig.Builder().apply {
            setLensFacing(lensFacing)
            // We request aspect ratio but no resolution to let CameraX optimize our use cases
            setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            setTargetRotation(viewFinder.display.rotation)
        }.build()

        // Use the auto-fit preview builder to automatically handle size and orientation changes
        preview = AutoFitPreviewBuilder.build(viewFinderConfig, viewFinder)

        // Set up the capture use case to allow users to take photos
        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setLensFacing(lensFacing)
            setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            // We request aspect ratio but no resolution to match preview config but letting
            // CameraX optimize for whatever specific resolution best fits requested capture mode
            setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            setTargetRotation(viewFinder.display.rotation)
        }.build()

        imageCapture = ImageCapture(imageCaptureConfig)

        // Setup image analysis pipeline that computes average pixel luminance in real time
        val analyzerConfig = ImageAnalysisConfig.Builder().apply {
            setLensFacing(lensFacing)
            // In our analysis, we care more about the latest image than analyzing *every* image
            setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            setTargetRotation(viewFinder.display.rotation)
        }.build()

        imageAnalyzer = ImageAnalysis(analyzerConfig).apply {
            analyzer = LuminosityAnalyzer {
                CaptureValidator.isCapture = true
                Log.d("TAG", "onLuminosityAnalyzerSuccess")
                imageAnalyzer?.removeAnalyzer()
                CameraX.unbindAll()
                uploadImage(bitmap = it)
//                Toast.makeText(context, "성공", Toast.LENGTH_SHORT).show()
            }
        }

        // Apply declared configs to CameraX using the same lifecycle owner
        CameraX.bindToLifecycle(
                viewLifecycleOwner, preview, imageCapture, imageAnalyzer)
    }

    /** Method used to re-draw the camera UI controls, called every time configuration changes */
    @SuppressLint("RestrictedApi")
    private fun updateCameraUi() {

        // Remove previous UI if any
        container.findViewById<ConstraintLayout>(R.id.camera_ui_container)?.let {
            container.removeView(it)
        }

        View.inflate(requireContext(), R.layout.camera_ui_container, container)
    }

    private fun uploadImage(bitmap: Bitmap) {
        val fileName = "record-" + System.currentTimeMillis() + ".png"
        val path = Environment.getExternalStorageDirectory().absolutePath + "/Picture/TimeChecker/"
        FileManager.saveBitmapToFileCache(bitmap, path, fileName) {
//            Toast.makeText(activity, "저장 성공", Toast.LENGTH_SHORT).show()
//            Intent().apply {
//                this.putExtra("path", path + fileName)
//            }.also { activity?.setResult(Activity.RESULT_OK, it) }
//            activity?.finish()

            val localPath = path + fileName
            Log.d("TAG", "success to save into local storage: $localPath")

            updateImageFile(bitmap, path, fileName) {
                Log.d("TAG", "onUpload Image")
                val file = urlToFile(localPath)
                val compressedFile = FileCompress.getCompressedImageFile(file, activity)
                compressedFile.let {
                    FileManager.uploadFile(fileName, it, object : FileManager.UploadCallback {
                        override fun onSuccess(url: String) {
//                            Toast.makeText(activity, "성공", Toast.LENGTH_SHORT).show()

                            val item = Intent().apply {
                                this.putExtra("image", url)
                            }
                            activity?.setResult(Activity.RESULT_OK, item)
                            activity?.finish()
                        }

                        override fun onFail() {
                            Toast.makeText(activity, "실패", Toast.LENGTH_SHORT).show()
                        }
                    })
                }
            }
        }
    }

    private fun urlToFile(url: String?): File {
        val splited = url?.split("/")
        val child = splited?.last()
        val parent = url?.substring(0, url.length - (child?.length ?: 0))
        Log.d("TAG", parent + ", " + child)
        return File(parent, child)
    }

    private fun updateImageFile(bitmap: Bitmap, strFilePath: String, filename: String, success: () -> Unit) {
        val file = File(strFilePath)

        if (!file.exists()) {
            file.mkdirs()
        }

        val fileCacheItem = File(strFilePath + filename)
        var `out`: OutputStream? = null

        try {
            fileCacheItem.createNewFile()
            `out` = FileOutputStream(fileCacheItem)

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, `out`)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                `out`?.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            success()
        }
    }

    /**
     * Our custom image analysis class.
     *
     * <p>All we need to do is override the function `analyze` with our desired operations. Here,
     * we compute the average luminosity of the image by looking at the Y plane of the YUV frame.
     */
    private class LuminosityAnalyzer(val success: (Bitmap)->Unit) : ImageAnalysis.Analyzer {

        private fun ByteBuffer.toByteArray(): ByteArray {
            rewind()    // Rewind the buffer to zero
            val data = ByteArray(remaining())
            get(data)   // Copy the buffer into a byte array
            return data // Return the byte array
        }

        private fun degreesToFirebaseRotation(degrees: Int): Int = when(degrees) {
            0 -> FirebaseVisionImageMetadata.ROTATION_0
            90 -> FirebaseVisionImageMetadata.ROTATION_90
            180 -> FirebaseVisionImageMetadata.ROTATION_180
            270 -> FirebaseVisionImageMetadata.ROTATION_270
            else -> throw Exception("Rotation must be 0, 90, 180, or 270.")
        }

        override fun analyze(imageProxy: ImageProxy?, degrees: Int) {
            if (CaptureValidator.isCapture) return

            val mediaImage = imageProxy?.image
            val imageRotation = degreesToFirebaseRotation(degrees)
            if (mediaImage != null) {
                val image = FirebaseVisionImage.fromMediaImage(mediaImage, imageRotation)
                val detector = CaptureValidator.provideDetector() ?: return
                detector.processImage(image)
                        .addOnSuccessListener { firebaseVisionText ->
                            if (CaptureValidator.isCapture) {
                                Log.d(TAG, "SuccessListener")
                                detector.close()
                                return@addOnSuccessListener
                            }

                            getTextBlocks(firebaseVisionText.textBlocks) { text ->
                                Log.d(TAG, text)
                                Log.d(TAG, "인식완료")
                                if (text.length > 6) {
                                    CaptureValidator.isCapture = true
                                    Log.d(TAG, "조건 충족")
                                    success(image.bitmap)
                                    detector.close()
                                    true
                                } else {
                                    false
                                }
                            }
                        }
                        .addOnFailureListener {
                            Log.d("TAG", "실패")
                        }
                        .addOnCompleteListener {
                            Log.d(TAG, "CompleteListener")
                            detector.close()
                        }
            }
        }

        private fun getTextBlocks(blocks: List<FirebaseVisionText.TextBlock>, callback: (String) -> Boolean) {
            var found: Boolean = false
            for (block in blocks) {
                for (line in block.lines) {
                    found = callback(line.text)
                    if (found) return
                }
            }
        }
    }

    companion object {
        private const val TAG = "CameraXBasic"
        private const val FILENAME = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val PHOTO_EXTENSION = ".jpg"

        /** Helper function used to create a timestamped file */
        private fun createFile(baseFolder: File, format: String, extension: String) =
                File(baseFolder, SimpleDateFormat(format, Locale.US)
                        .format(System.currentTimeMillis()) + extension)

        /** Use external media if it is available, our app's file directory otherwise */
        fun getOutputDirectory(context: Context): File {
            val appContext = context.applicationContext
            val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
                File(it, appContext.resources.getString(R.string.app_name)).apply { mkdirs() }
            }
            return if (mediaDir != null && mediaDir.exists())
                mediaDir else appContext.filesDir
        }

    }
}
