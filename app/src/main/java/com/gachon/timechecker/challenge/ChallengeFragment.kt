package com.gachon.timechecker.challenge

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.gachon.timechecker.R
import com.gachon.timechecker.common.CaptureValidator
import com.gachon.timechecker.common.ExtensionFragment
import com.gachon.timechecker.common.getElapsedTime
import com.gachon.timechecker.common.getTime
import com.gachon.timechecker.data.Challenge
import com.gachon.timechecker.data.ChallengeRecord
import com.gachon.timechecker.data.WeekDay
import com.gachon.timechecker.detailphoto.DetailPhotosActivity
import com.gachon.timechecker.repository.ChallengeRepository
import kotlinx.coroutines.*
import java.util.*

class ChallengeFragment : ExtensionFragment() {
    private val REQUEST_CODE_CAMERA_START = 1001
    private val REQUEST_CODE_CAMERA_END = 1002

    val REQUEST_PERMISSIONS_PICTURES_RELATED = 20001

    private var _currentChallenge: Challenge? = null

    private var viewHolder: ChallengeViewHolder? = null

    private var isDone = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_challenge, container, false)

        viewHolder = ChallengeViewHolder(v)

        startProgress()

        fetchData(viewHolder!!)

        return v
    }

    private fun fetchData(holder: ChallengeViewHolder) {
        ChallengeRepository.getChallenges { challenges ->
            val todo = challenges.filter { !it.isDone }.sortedBy { it.time }
            if (todo.isEmpty()) {
                endProgress()
                holder.layoutEmpty.visibility = View.VISIBLE
                return@getChallenges
            }

            _currentChallenge = todo.first()

            ChallengeRepository.checkIfStart(_currentChallenge!!) result@{
                if (context == null) {
                    return@result
                }

                bindChallenge(holder, _currentChallenge)

                when (it) {
                    is ChallengeRepository.Status.Start -> {
                        holder.btnCheckEnd.isEnabled = false
                    }

                    is ChallengeRepository.Status.End -> {
                        holder.btnCheckStart.isEnabled = false

                        setTimer(holder, it.leftTime)
                        holder.btnCheckStart.text = ""
                        holder.btnCheckStart.setBackgroundResource(R.drawable.ic_check_circle_black_24dp)
                    }

                    is ChallengeRepository.Status.Done -> {
                        holder.btnCheckStart.isEnabled = false
                        holder.btnCheckEnd.isEnabled = false

                        isDone = true
                        holder.btnCheckStart.text = ""
                        holder.btnCheckEnd.text = ""
                        holder.btnCheckStart.setBackgroundResource(R.drawable.ic_check_circle_black_24dp)
                        holder.btnCheckEnd.setBackgroundResource(R.drawable.ic_check_circle_black_24dp)
                    }
                }

                endProgress()
            }
        }
    }

    private fun bindChallenge(holder: ChallengeViewHolder, challengeToDo: Challenge?) {
        if (challengeToDo == null) {
            Toast.makeText(activity, "도전에 대한 정보를 가져오지 못했습니다.", Toast.LENGTH_SHORT).show()
            return
        }

        bindTitle(holder, challengeToDo)

        val infoString = "인증 가능시간\n${challengeToDo.weeks}요일\n${challengeToDo.startTime} ~ ${challengeToDo.endTime}"
        holder.tvCheckableTime.text = infoString

        bindCheckBtn(holder, challengeToDo)

        bindCurrentStatus(holder, challengeToDo)

        bindChallengeInfo(holder, challengeToDo)
    }

    private fun bindCurrentStatus(holder: ChallengeViewHolder, challenge: Challenge) {
        ChallengeRepository.getCurrentChallengeStatus(challenge) {
            if (challenge.willCount == 0) {
                holder.tvCurrentCheckStatus.text = "0/0 (0.0%)"
                return@getCurrentChallengeStatus
            }

            val percentage = (it.size.toFloat() / challenge.willCount * 100).toInt()
            val infoMessage = "${it.size} / ${challenge.willCount} ($percentage%)"
            holder.tvCurrentCheckStatus.text = infoMessage

            val photos = extractOnlyPhotos(it)
            moveToDetailPhotos(holder, percentage, photos)
        }
    }

    private fun extractOnlyPhotos(records: List<ChallengeRecord>) = records.sortedByDescending { it.id }
            .map { it.url ?: "" }
            .toList()

    private fun moveToDetailPhotos(holder: ChallengeViewHolder, percentage: Int, photos: List<String>) {
        holder.btnCurrentCheckStatus.setOnClickListener {
            val title = holder.tvTitleChallenge.text.toString() + "($percentage%)"
            onMoveToDetailPhotos(title, photos)
        }
    }

    private fun onMoveToDetailPhotos(title: String, photos: List<String>) {
        Intent(activity, DetailPhotosActivity::class.java).apply {
            val photosList = arrayListOf<String>().apply {
                addAll(photos)
            }

            this.putStringArrayListExtra("photos", photosList)
            this.putExtra("title", title)
        }.also { startActivity(it) }
    }

    private fun bindChallengeInfo(holder: ChallengeViewHolder, challenge: Challenge) {
        val time = getElapsedTime(challenge)
        holder.tvInputWeekday.text = challenge.weeks
        holder.tvInputCheckTime.text = challenge.startTime + " ~ " + challenge.endTime
        holder.tvInputStudyTime.text = if (time.second == 0) {
            "하루 ${time.first}시간"
        } else {
            "하루 ${time.first}시간 ${time.second}분"
        }
    }

    private fun bindCheckBtn(holder: ChallengeViewHolder, challenge: Challenge) {
        holder.btnCheckStart.setOnClickListener {
            if (!checkPermission()) {
                val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                ActivityCompat.requestPermissions(activity!!, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
            }

            if (isNotInCheckWeekDay(challenge)) {
                Toast.makeText(context, "인증 요일이 아닙니다.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (isNotInCheckTime(challenge)) {
                Toast.makeText(context, "인증 시간이 아닙니다.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            moveToCamera("start")
        }

        holder.btnCheckEnd.setOnClickListener {
            if (!checkPermission()) {
                val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                ActivityCompat.requestPermissions(activity!!, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
            }

            if (isNotInCheckWeekDay(challenge)) {
                Toast.makeText(context, "인증 요일이 아닙니다.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (isNotInCheckTime(challenge)) {
                Toast.makeText(context, "인증 시간이 아닙니다.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            moveToCamera("end")
        }
    }

    private fun isNotInCheckWeekDay(challenge: Challenge): Boolean {
        val current = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        return challenge.weeks.split(", ")
                .map { WeekDay.from(it) }
                .all { it.ordinal != (current-1) }
    }

    private fun isNotInCheckTime(challenge: Challenge): Boolean {
        val start = toCalendar(challenge.startTime)
        val end = toCalendar(challenge.endTime)

        val current = Calendar.getInstance()
        return current.before(start) || current.after(end)
    }

    private fun moveToCamera(type: String) {
        CaptureValidator.isCapture = false

        Intent(activity, CameraActivity::class.java).apply {
        }.also {
            startActivityForResult(it, when (type) {
                "start" -> REQUEST_CODE_CAMERA_START
                else -> REQUEST_CODE_CAMERA_END
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }

        val type = when (requestCode) {
            REQUEST_CODE_CAMERA_START -> "start"
            REQUEST_CODE_CAMERA_END -> "end"
            else -> throw IllegalStateException()
        }

        startProgress()

        val url = data?.getStringExtra("image")
        val thisTime = Calendar.getInstance()
        applyToCheck(_currentChallenge, thisTime.timeInMillis, type, url) result@{
            if (viewHolder == null) {
                return@result
            }

            fetchData(viewHolder!!)
        }
    }

    private fun applyToCheck(challenge: Challenge?, currentTime: Long, type: String, url: String?, callback: () -> Unit) {
        if (challenge == null) {
            Toast.makeText(activity, "도전에 대한 정보를 가져오지 못했습니다.", Toast.LENGTH_SHORT).show()
            return
        }

        ChallengeRepository.recordCheckChallenge(challenge, currentTime, type, url) {
            Toast.makeText(context, "등록되었습니다.", Toast.LENGTH_SHORT).show()
            callback()
        }
    }

    private fun bindTitle(holder: ChallengeViewHolder, challengeToDo: Challenge) {
        val time = getElapsedTime(challengeToDo)
        holder.tvTitleChallenge.text = if (time.second == 0) {
            "토익 ${time.first}시간 공부하기"
        } else {
            "토익 ${time.first}시간 ${time.second}분 공부하기"
        }
    }

    private fun toCalendar(time: String): Calendar {
        val rawTime = time.split(":").map { it.toInt() }
        return getTime(rawTime[0], rawTime[1])
    }

    private fun setTimer(holder: ChallengeViewHolder, leftTime :Long) {
        isDone = false
        val leftRaw = leftTime / 1000
        var leftMin = (leftRaw / 60).toInt()
        var leftSec = (leftRaw % 60).toInt()

        startTimer(1000) {
            if (leftSec == 0) {
                leftMin -= 1
                leftSec = 59
            } else {
                leftSec -= 1
            }

            CoroutineScope(Dispatchers.Main).launch {
                if (isDone) {
                    return@launch
                }

                holder.btnCheckEnd.text = "${to2Digit(leftMin)}:${to2Digit(leftSec)}"
            }

            (leftMin == 0 && leftSec == 0) || isDone
        }
//        btn_check_end.text = "00:00"
    }

    private fun to2Digit(digit: Int): String {
        if (digit < 10) {
            return "0$digit"
        }

        return digit.toString()
    }

    private fun startTimer(repeatMillis: Long = 0, action: () -> Boolean) = GlobalScope.launch {
        if (repeatMillis > 0) {
            while (true) {
                if (action()) cancel()
                delay(repeatMillis)
            }
        } else {
            if (action()) cancel()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                ChallengeFragment()
    }

    class ChallengeViewHolder(view: View) {
        val tvTitleChallenge by lazy { view.findViewById<TextView>(R.id.tv_title_challenge) }
        val tvCheckableTime by lazy { view.findViewById<TextView>(R.id.tv_checkable_time) }
        //    android:text="인증 가능시간\n수요일\n20:00~20:05"

        val btnCheckStart by lazy { view.findViewById<Button>(R.id.btn_check_start) }
        val btnCheckEnd by lazy { view.findViewById<Button>(R.id.btn_check_end) }

        val btnCurrentCheckStatus by lazy { view.findViewById<LinearLayout>(R.id.btn_challenge_percent) }
        val tvCurrentCheckStatus by lazy { view.findViewById<TextView>(R.id.tv_percentage) }
        //    android:text="2/20 (10.0%)"

        val tvInputWeekday by lazy { view.findViewById<TextView>(R.id.tv_input_weekday) }
        val tvInputStudyTime by lazy { view.findViewById<TextView>(R.id.tv_input_study_time) }
        val tvInputCheckTime by lazy { view.findViewById<TextView>(R.id.tv_input_check_time) }

        val layoutEmpty by lazy { view.findViewById<LinearLayout>(R.id.layout_empty) }
    }

    private fun checkPermission(): Boolean {
        if (context == null) return false

        val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity!!, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
                false
            } else {
                true
            }
        }
        return true
    }
}
