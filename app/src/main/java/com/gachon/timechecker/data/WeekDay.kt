package com.gachon.timechecker.data

enum class WeekDay(val ko: String) {
    Sun("일"),
    Mon("월"),
    Tue("화"),
    Wed("수"),
    Thur("목"),
    Fri("금"),
    Sat("토");

    companion object {
        fun from(ko: String): WeekDay {
            return when (ko) {
                "월" -> WeekDay.Mon
                "화" -> WeekDay.Tue
                "수" -> WeekDay.Wed
                "목" -> WeekDay.Thur
                "금" -> WeekDay.Fri
                "토" -> WeekDay.Sat
                "일" -> WeekDay.Sun
                else -> throw IllegalStateException()
            }
        }
    }
}
