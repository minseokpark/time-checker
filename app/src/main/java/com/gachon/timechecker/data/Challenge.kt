package com.gachon.timechecker.data

import com.gachon.timechecker.GlobalDef
import com.google.gson.annotations.SerializedName

/**
 * Created by minseok on 2019-09-29.
 * TimeChecker.
 */
data class Challenge(
        @SerializedName(GlobalDef.CHALLENGE_ID) val id: Int,
        @SerializedName(GlobalDef.CHALLENGE_USER) val user: String,
        @SerializedName(GlobalDef.CHALLENGE_WEEKS) val weeks: String,
        @SerializedName(GlobalDef.CHALLENGE_START_TIME) val startTime: String,
        @SerializedName(GlobalDef.CHALLENGE_END_TIME) val endTime: String,
        @SerializedName(GlobalDef.CHALLENGE_CURRENT_TIME) var time: Long,
        @SerializedName(GlobalDef.CHALLENGE_WILL_COUNT) var willCount: Int = 0,
        @SerializedName(GlobalDef.CHALLENGE_IS_DONE) var isDone: Boolean = false
) {

    constructor() : this(-1, "", "", "", "",  0, 0, false)
}