package com.gachon.timechecker.data

import com.gachon.timechecker.GlobalDef
import com.google.gson.annotations.SerializedName

/**
 * Created by minseok on 2019-09-26.
 * TimeChecker.
 */
data class Comment(
        @SerializedName(GlobalDef.COMMENT_ID) val id: Int,
        @SerializedName(GlobalDef.COMMENT_POST_ID) val postId: Int,
        @SerializedName(GlobalDef.COMMENT_AUTHOR) val userName: String,
        @SerializedName(GlobalDef.COMMENT_AUTHOR_PHOTO) val photo: String,
        @SerializedName(GlobalDef.COMMENT_CONTENT) val content: String
) {

    constructor() : this(-1, -1, "", "", "")
}