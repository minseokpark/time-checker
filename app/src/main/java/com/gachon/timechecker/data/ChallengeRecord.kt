package com.gachon.timechecker.data

import com.gachon.timechecker.GlobalDef
import com.google.gson.annotations.SerializedName

/**
 * Created by minseok on 2019-09-29.
 * TimeChecker.
 */
data class ChallengeRecord(
        @SerializedName(GlobalDef.CHALLENGE_RECORD_ID) val id: Int,
        @SerializedName(GlobalDef.CHALLENGE_RECORD_RECORDED_TYPE) var type: String? = null,
        @SerializedName(GlobalDef.CHALLENGE_RECORD_RECORDED_TIME) var time: Long? = 0L,
        @SerializedName(GlobalDef.CHALLENGE_RECORD_CHALLENGE_ID) var challengeId: Int = 0,
        @SerializedName(GlobalDef.CHALLENGE_RECORD_IMAGE_URL) var url: String? = null
) {

    constructor() : this(-1, "", 0L, 0, "")
}