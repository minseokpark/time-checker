package com.gachon.timechecker.data

import com.gachon.timechecker.GlobalDef
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by minseok on 2019-09-26.
 * TimeChecker.
 */
data class Post(
        @SerializedName(GlobalDef.POST_ID) val id: Int,
        @SerializedName(GlobalDef.POST_SUBJECT) val subject: String,
        @SerializedName(GlobalDef.POST_CONTENT) val content: String,
        @SerializedName(GlobalDef.POST_AUTHOR) val author: String,
        @SerializedName(GlobalDef.POST_AUTHOR_IMAGE) val authorPhoto: String,
        @SerializedName(GlobalDef.POST_PHOTO) val photo: String?
) : Serializable {

    constructor() : this(-1, "", "", "", "", null)
}