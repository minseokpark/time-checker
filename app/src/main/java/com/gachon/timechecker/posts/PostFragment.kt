package com.gachon.timechecker.posts

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.gachon.timechecker.R
import com.gachon.timechecker.data.Post
import com.gachon.timechecker.repository.PostRepository
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_post.*

class PostFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post, container, false).apply {
            this.findViewById<FloatingActionButton>(R.id.btn_post).apply {
                this.setOnClickListener { moveWritePost() }
            }
        }.also { getPosts() }
    }

    private fun getPosts() {
        PostRepository.getAllPosts { list ->
            if (container_posts == null) {
                return@getAllPosts
            }

            list.forEach { container_posts.addView(getView(it)) }
        }
    }

    private fun getView(post: Post): View? {
        if (context == null) return null

        val view = ConstraintLayout.inflate(context, R.layout.item_post, null).apply {
            this.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        }

        with(view) {
            findViewById<TextView>(R.id.tv_subject).text = post.subject
            findViewById<TextView>(R.id.tv_author).text = post.author
            findViewById<ImageView>(R.id.iv_profile).also {
                Glide.with(this).load(post.authorPhoto).into(it)
            }
            this.setOnClickListener {
                moveDetailPost(post.id)
            }
        }

        return view
    }

    private fun moveWritePost() {
        Intent(context, WritePostActivity::class.java).also {
            startActivityForResult(it, REQUEST_CODE_WRITING)
        }
    }

    private fun moveDetailPost(postNo: Int) {
        Intent(context, DetailPostActivity::class.java).apply {
            this.putExtra("postNo", postNo)
        }.also { startActivity(it) }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_WRITING) {
            container_posts.removeAllViews()
            getPosts()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = PostFragment()

        public val REQUEST_CODE_WRITING = 1000
    }
}
