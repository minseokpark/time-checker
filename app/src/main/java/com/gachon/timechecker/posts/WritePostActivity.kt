package com.gachon.timechecker.posts

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.gachon.timechecker.R
import com.gachon.timechecker.common.ExtensionActivity
import com.gachon.timechecker.common.FileCompress
import com.gachon.timechecker.common.FileManager
import com.gachon.timechecker.common.setImage
import com.gachon.timechecker.repository.PostRepository
import kotlinx.android.synthetic.main.activity_write_post.*
import java.io.File

class WritePostActivity : ExtensionActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_post)

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(false)
            it.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { onBackPressed() }
        }

        ibtn_camera.setOnClickListener {
            if (checkPermission()) {
                onStartPictures()
            } else {
                val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                ActivityCompat.requestPermissions(this@WritePostActivity, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
            }
        }

        ibtn_submit.setOnClickListener {
            val subject = edit_subject.text.toString()
            val content = edit_content.text.toString()

            if (subject.isEmpty()) {
                Toast.makeText(this@WritePostActivity, "제목을 넣어주세요.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (content.isEmpty()) {
                Toast.makeText(this@WritePostActivity, "내용을 넣어주세요.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            PostRepository.addPost(subject, content, urlForUpload) {
                Toast.makeText(this, "성공적으로 업로드 되었습니다.", Toast.LENGTH_SHORT).show()
                setResult(PostFragment.REQUEST_CODE_WRITING)
                finish()
            }
        }
    }

    val TAG_CAMERA: Int = 10001
    val REQUEST_PERMISSIONS_PICTURES_RELATED = 20001
    var urlForUpload = ""

    private var mImageFile: File? = null

    private fun onStartPictures() {
        val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(Intent.createChooser(cameraIntent, "Select Picture"), TAG_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        startProgress()

        when (requestCode) {
            TAG_CAMERA -> {
                if (resultCode != Activity.RESULT_OK) {
                    return
                }

                val uri = data?.data
                if (uri != null) {
                    mImageFile = uriToImageFile(uri)
                    val fileName = "post-" + System.currentTimeMillis() + ".png"
                    val compressed = FileCompress.getCompressedImageFile(mImageFile, this)
                    FileManager.uploadFile(fileName, compressed!!, object : FileManager.UploadCallback {
                        override fun onSuccess(url: String) {
                            urlForUpload = url
                            iv_upload.setImage(url)
//                            this.let { Glide.with(this@WritePostActivity).load(mImageFile).into(iv_upload) }
                            endProgress()
                        }

                        override fun onFail() {
                            Toast.makeText(this@WritePostActivity, "이미지 업로드에 실패하였습니다.", Toast.LENGTH_SHORT).show()
                            endProgress()
                        }
                    })
                }
            }
        }
    }

    private fun checkPermission(): Boolean {
        val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
                false
            } else {
                true
            }
        }
        return true
    }

    private fun uriToImageFile(uri: Uri): File? {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = this.contentResolver?.query(uri, filePathColumn, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val filePath = cursor.getString(columnIndex)
                cursor.close()
                return File(filePath)
            }
            cursor.close()
        }
        return null
    }

}
