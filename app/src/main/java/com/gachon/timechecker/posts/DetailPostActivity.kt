package com.gachon.timechecker.posts

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.gachon.timechecker.R
import com.gachon.timechecker.common.ExtensionActivity
import com.gachon.timechecker.common.setImage
import com.gachon.timechecker.data.Comment
import com.gachon.timechecker.detailphoto.DetailPhotoActivity
import com.gachon.timechecker.repository.PostRepository
import kotlinx.android.synthetic.main.activity_detail_post.*

class DetailPostActivity : ExtensionActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_post)

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(false)
            it.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { onBackPressed() }
        }

        val postNo = intent.getIntExtra("postNo", -1)
        if (postNo == -1 ) {
            Toast.makeText(this, "게시글을 찾을 수 없습니다.", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        PostRepository.getPostInfo(postNo) { post ->
            tv_subject.text = post.subject
            tv_content.text = post.content
            Glide.with(this@DetailPostActivity).load(post.authorPhoto).into(iv_profile)

            post.photo?.let {
                iv_image.visibility = View.VISIBLE
//                Glide.with(this@DetailPostActivity).load(it).into(iv_image)
                iv_image.setImage(it)
            }

            iv_image.setOnClickListener {
                Intent(this@DetailPostActivity, DetailPhotoActivity::class.java).apply {
                    this.putExtra("url", post.photo)
                    this.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
                }.also { startActivity(it) }
            }
        }

        PostRepository.getComments(postNo) { comments ->
            comments.map { getCommentView(it) }
                    .forEach { container_comments.addView(it) }
        }

        btn_submit.setOnClickListener {
            startProgress()
            val edit = findViewById<EditText>(R.id.edit_comment)
            val content = edit.text.toString()
            submitComment(postNo, content) {
                endProgress()
                edit.setText("")
            }
        }
    }

    private fun submitComment(postNo: Int, content: String, callback: () -> Unit) {
        if (content.isEmpty()) {
            Toast.makeText(this, "댓글을 입력해주세요.", Toast.LENGTH_SHORT).show()
            return
        }

        PostRepository.addComment(postNo, content) { comment ->
            container_comments.addView(getCommentView(comment))

            callback()
        }
    }

    private fun getCommentView(comment: Comment): View {
        val view = ConstraintLayout.inflate(this@DetailPostActivity, R.layout.item_comment, null)

        with(view) {
            this.findViewById<TextView>(R.id.tv_name).text = comment.userName
            this.findViewById<TextView>(R.id.tv_content).text = comment.content

            val iv = this.findViewById<ImageView>(R.id.iv_profile)
            Glide.with(this@DetailPostActivity).load(comment.photo).into(iv)
        }

        return view
    }
}
