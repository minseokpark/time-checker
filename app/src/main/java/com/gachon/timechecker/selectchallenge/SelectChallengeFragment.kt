package com.gachon.timechecker.selectchallenge

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.gachon.timechecker.GlobalDef
import com.gachon.timechecker.R

class SelectChallengeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_select_challenge, container, false).also { it ->
            initView(it)
        }
    }

    private fun initView(view: View?) {
        if (view == null) return

        // 매일
        view.findViewById<CardView>(R.id.container_topleft).setOnClickListener {
            Intent(context, SelectChallengeDetailActivity::class.java).apply {
                this.putExtra("weekday", "daily")
            }.also { startActivity(it) }
        }

        // 주 3회
        view.findViewById<CardView>(R.id.container_topright).setOnClickListener {
            Intent(context, SelectChallengeDetailActivity::class.java).apply {
                this.putExtra("weekday", "weekthree")
            }.also { startActivity(it)}
        }

        // 주 5회 (평일)
        view.findViewById<CardView>(R.id.container_bottomleft).setOnClickListener {
            Intent(context, SelectChallengeDetailActivity::class.java).apply {
                this.putExtra("weekday", "week")
            }.also { startActivity(it)}
        }


        // 주 2회 (주말)
        view.findViewById<CardView>(R.id.container_bottomright).setOnClickListener {
            Intent(context, SelectChallengeDetailActivity::class.java).apply {
                this.putExtra("weekday", "weekend")
            }.also { startActivity(it)}
        }

        Glide.with(this).load(GlobalDef.URL_TOEIC).into(view.findViewById<ImageView>(R.id.iv_topleft))
        Glide.with(this).load(GlobalDef.URL_TOEIC).into(view.findViewById<ImageView>(R.id.iv_topright))
        Glide.with(this).load(GlobalDef.URL_TOEIC).into(view.findViewById<ImageView>(R.id.iv_bottomleft))
        Glide.with(this).load(GlobalDef.URL_TOEIC).into(view.findViewById<ImageView>(R.id.iv_bottomright))
    }

    companion object {

        @JvmStatic
        fun newInstance() = SelectChallengeFragment()
    }
}
