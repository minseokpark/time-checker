package com.gachon.timechecker.selectchallenge

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.gachon.timechecker.GlobalDef
import com.gachon.timechecker.R
import com.gachon.timechecker.common.ExtensionActivity
import com.gachon.timechecker.repository.ChallengeRepository
import kotlinx.android.synthetic.main.activity_select_challenge_detail.*
import java.text.SimpleDateFormat
import java.util.*

class SelectChallengeDetailActivity : ExtensionActivity() {
    private var _startHour: Int = -1
    private var _startMin: Int = -1
    private var _endHour: Int = -1
    private var _endMin: Int = -1

    private var targetSelected = 7
    private var currentSelectedWeek = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_challenge_detail)

        btn_request_challenge.setOnClickListener { submitChallenge() }

        val limitedWeekDay = intent.getStringExtra("weekday")

        setWeekdayInit(limitedWeekDay)

        setWeekdayListener()

        setStudyTime()

        notifyAllInfo()

        Glide.with(this).load(GlobalDef.URL_TOEIC).into(logo_toeic)
    }

    private fun setWeekdayListener() {

        toggle_mon.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_tue.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_wen.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_thur.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_fri.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_sat.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
        toggle_sun.setOnCheckedChangeListener { _, _ ->
            notifyAllInfo()
        }
    }

    private fun setStudyTime() {
        tv_start.setOnClickListener {
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, hour, min ->
                if (checkValidatedDate(hour, min, _endHour, _endMin)) {
                    Toast.makeText(this, "시작 시간이 종료시간보다 늦을 수 없습니다.", Toast.LENGTH_SHORT).show()
                    return@OnTimeSetListener
                }

                _startHour = hour
                _startMin = min

                tv_start.text = getTimeString(hour, min)

                notifyAllInfo()
            }, _startHour, _startMin, true).show()
        }

        tv_end.setOnClickListener {
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, hour, min ->
                if (checkValidatedDate(_startHour, _startMin, hour, min)) {
                    Toast.makeText(this, "시작 시간이 종료시간보다 늦을 수 없습니다.", Toast.LENGTH_SHORT).show()
                    return@OnTimeSetListener
                }

                _endHour = hour
                _endMin = min

                tv_end.text = getTimeString(hour, min)

                notifyAllInfo()
            }, _endHour, _endMin, true).show()
        }
    }

    private fun checkValidatedDate(_startHour: Int, _startMin: Int, _endHour: Int, _endMin: Int): Boolean {
        if (_startHour == -1) return false
        if (_endHour == -1) return false

        val start = getTime(_startHour, _startMin)
        val end = getTime(_endHour, _endMin)

        return start.after(end)
    }

    private fun setWeekdayInit(weekDay: String) {
        when (weekDay) {
            "week" -> {
                tv_status.text = "주 5일 (평일)"

                toggle_sat.visibility = View.GONE
                toggle_sun.visibility = View.GONE

                toggle_mon.isEnabled = false
                toggle_tue.isEnabled = false
                toggle_wen.isEnabled = false
                toggle_thur.isEnabled = false
                toggle_fri.isEnabled = false
                toggle_sat.isEnabled = false
                toggle_sun.isEnabled = false

                toggle_mon.isEnabled = false
                toggle_tue.isEnabled = false
                toggle_wen.isEnabled = false
                toggle_thur.isEnabled = false
                toggle_fri.isEnabled = false

                toggle_mon.isChecked = true
                toggle_tue.isChecked = true
                toggle_wen.isChecked = true
                toggle_thur.isChecked = true
                toggle_fri.isChecked = true

                targetSelected = 5
            }

            "weekthree" -> {
                tv_status.text = "주 3일 (선택)"

                targetSelected = 3
            }

            "weekend" -> {
                tv_status.text = "주 2일 (주말)"

                toggle_mon.isEnabled = false
                toggle_tue.isEnabled = false
                toggle_wen.isEnabled = false
                toggle_thur.isEnabled = false
                toggle_fri.isEnabled = false
                toggle_sat.isEnabled = false
                toggle_sun.isEnabled = false

                toggle_sat.isChecked = true
                toggle_sun.isChecked = true

                targetSelected = 2
            }

            "daily" -> {
                tv_status.text = "매일"

                toggle_mon.isEnabled = false
                toggle_tue.isEnabled = false
                toggle_wen.isEnabled = false
                toggle_thur.isEnabled = false
                toggle_fri.isEnabled = false
                toggle_sat.isEnabled = false
                toggle_sun.isEnabled = false

                toggle_mon.isChecked = true
                toggle_tue.isChecked = true
                toggle_wen.isChecked = true
                toggle_thur.isChecked = true
                toggle_fri.isChecked = true
                toggle_sat.isChecked = true
                toggle_sun.isChecked = true
            }
        }
    }

    private fun notifyAllInfo() {
        // initWeekDay
        currentSelectedWeek = 0
        val weekList = arrayListOf<String>()
        if (toggle_mon.isChecked) {
            weekList.add("월")
            currentSelectedWeek++
        }

        if (toggle_tue.isChecked) {
            weekList.add("화")
            currentSelectedWeek++
        }

        if (toggle_wen.isChecked) {
            weekList.add("수")
            currentSelectedWeek++
        }

        if (toggle_thur.isChecked) {
            weekList.add("목")
            currentSelectedWeek++
        }

        if (toggle_fri.isChecked) {
            weekList.add("금")
            currentSelectedWeek++
        }

        if (toggle_sat.isChecked) {
            weekList.add("토")
            currentSelectedWeek++
        }

        if (toggle_sun.isChecked) {
            weekList.add("일")
            currentSelectedWeek++
        }

        tv_info_dayweek.text = weekList.joinToString()

        // initTime

        val start = if (_startHour == -1) "00:00" else getTimeString(_startHour, _startMin)
        val end = if (_endHour == -1) "00:00" else getTimeString(_endHour, _endMin)
        tv_info_target_time.text = "$start ~ $end"

        val startTime = getTime(_startHour, _startMin)
        val endTime = getTime(_endHour, _endMin)
        val timeMillis = (endTime.timeInMillis - startTime.timeInMillis) / 1000

        val hour = timeMillis / 3600
        val min = (timeMillis % 3600) / 60
        if (_startHour != -1 && _endHour != -1) {
            tv_info_study_time.text = "하루 ${hour}시간 ${min}분"
        }

        btn_request_challenge.isEnabled = (currentSelectedWeek == targetSelected) && (_startHour != -1 && _endHour != -1)
    }

    private fun getTimeString(hour: Int, min: Int) = SimpleDateFormat("HH:mm").format(getTime(hour, min).time)

    private fun getTime(hour: Int, min: Int): Calendar {
        return Calendar.getInstance().apply {
            this.set(Calendar.HOUR_OF_DAY, hour)
            this.set(Calendar.MINUTE, min)
        }
    }

    private fun submitChallenge() {
        val weeks = tv_info_dayweek.text.toString()
        val timeList = tv_info_target_time.text.split(" ~ ")

        val startTime = timeList[0]
        val endTime = timeList[1]

        startProgress()
        ChallengeRepository.requestChallenge(weeks, startTime, endTime,
                success = {
                    finish()
                }, failure = {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }, finish = {
            endProgress()
        })
    }
}
