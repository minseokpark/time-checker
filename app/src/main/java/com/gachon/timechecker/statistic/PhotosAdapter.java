package com.gachon.timechecker.statistic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gachon.timechecker.R;
import com.gachon.timechecker.common.FileManager;
import com.gachon.timechecker.detailphoto.DetailPhotoActivity;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * Created by minseok on 2019-10-24.
 * TimeChecker.
 */
public class PhotosAdapter extends BaseAdapter {
    private Context context;
    private List<String> items;
    private int max = -1;
    private int width = -1;

    public PhotosAdapter(Context context, List<String> items, int width) {
        this.context = context;
        this.items = items;

        this.width = width;
    }

    public PhotosAdapter(Context context, List<String> items, int width, int max) {
        this.context = context;
        this.items = items;

        this.width = width;
        this.max = max;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = ConstraintLayout.inflate(context, R.layout.item_photo, null);
        view.setLayoutParams(new ConstraintLayout.LayoutParams(width, width));

        ImageView iv = view.findViewById(R.id.iv);

        RequestOptions req = new RequestOptions();
        req.placeholder(R.drawable.sample);
        String url = items.get(position);
        if (url.contains("gs://")) {
            FileManager.Companion.toUri(url, uri ->  {
                if (((Activity) context).isFinishing()) {
                    return;
                }
                Glide.with(context).load(uri).into(iv);
            });
        } else {
            Glide.with(context).load(url).into(iv);
        }

        iv.setOnClickListener(v -> {
            Intent intent = new Intent(context, DetailPhotoActivity.class);
            intent.putExtra("url", url);
            context.startActivity(intent);
        });

        return view;
    }

    @Override
    public int getCount() {
//        return (max != -1) ?  max : items.size();
        return items.size();
    }

    @Override
    public String getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static int dpTopx(int dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }
}
