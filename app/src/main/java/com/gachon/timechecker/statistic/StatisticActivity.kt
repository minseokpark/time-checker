package com.gachon.timechecker.statistic

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import com.gachon.timechecker.R
import com.gachon.timechecker.common.ExtensionActivity
import com.gachon.timechecker.common.setImage
import com.gachon.timechecker.data.ChallengeRecord
import com.gachon.timechecker.detailphoto.DetailPhotosActivity
import com.gachon.timechecker.repository.ChallengeRepository
import com.gachon.timechecker.repository.UserSetting
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.activity_statistic.*

class StatisticActivity : ExtensionActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(false)
            it.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationOnClickListener { onBackPressed() }
        }

        val title = intent.getStringExtra("title")
        val weeks = intent.getStringExtra("weeks")
        val startTime = intent.getLongExtra("startTime", 0L)
        val countPerWeeks = weeks.split(", ").size
        val willCount = intent.getIntExtra("willCount", -1)
        val challengeId = intent.getIntExtra("challengeId", -1)

        tv_title.text = title
        my.setImage(UserSetting.userPic)

        Log.d("TAG", "ChallengeId: " + challengeId)

        startProgress()
        bindStatistic(challengeId, countPerWeeks, willCount, startTime)
    }

    private fun bindStatistic(challengeId: Int, countPerWeeks: Int, willCount: Int, startTime: Long) {
        ChallengeRepository.getChallengeRecords(challengeId) {
            val weekStatistic = getWeek(it, countPerWeeks, startTime)
            bindWeekChart(weekStatistic)

            val myPercentage = getMyPercentage(it, willCount)
            Log.d("TAG", myPercentage.toString())

            initPhotos(getMyPhotos(it))

            ChallengeRepository.getChallengeTotalPercentage { totalChallenges ->
                // 전체 도전
                val totalPercentage = totalChallenges
                // 도전에 대한 기록 검색
                ChallengeRepository.getChallengeRecordTotalPercentage { totalRecords ->
                    val countMap = totalRecords.groupBy { it.challengeId }
                    // 기록 개수 / willCount * 100
                    var totalPercentageValue = 1.0F
                    totalPercentage.forEach {
                        val value = getMyPercentage(countMap[it.id] ?: emptyList(), it.willCount)

                        val result = if (value == 0) 1 else value
                        totalPercentageValue *= result.toFloat() / 100
                    }
                    totalPercentageValue *= 100

                    bindPercentageChart(myPercentage, totalPercentageValue.toInt())
                    endProgress()
                }
            }
        }
    }

    private fun bindWeekChart(weekStatistic: List<Int>) {
        val chart = chart_weeks_complete
        chart.description.isEnabled = false
        chart.setTouchEnabled(false)

        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        chart.axisLeft.setDrawLabels(false)
        chart.axisLeft.setDrawGridLines(false)
        chart.axisLeft.setDrawAxisLine(false)
        chart.axisRight.setDrawLabels(false)
        chart.axisRight.setDrawGridLines(false)
        chart.axisRight.setDrawAxisLine(false)

        val weeksLabel = IntRange(1, weekStatistic.size).map { "${it}주차" }
        chart.xAxis.valueFormatter = IndexAxisValueFormatter(weeksLabel)
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.setDrawAxisLine(false)
        chart.xAxis.setLabelCount(weeksLabel.size, false)
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.legend.isEnabled = false
        chart.legend.textSize = 0F

        val values = ArrayList<BarEntry>().apply {
            weekStatistic.forEachIndexed { index, value ->
                this.add(BarEntry(index.toFloat(), value.toFloat()))
            }
        }

        val set1 = BarDataSet(values, "").apply {
            this.setColors(*ColorTemplate.VORDIPLOM_COLORS)
            this.setDrawValues(true)
            this.valueTextSize = 12F
            this.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return value.toInt().toString() + "%"
                }
            }
        }

        chart.data = BarData(set1)
        chart.setFitBars(true)

        chart.invalidate()
    }

    private fun bindPercentageChart(myPercentage: Int, others: Int) {
        val chart = chart_compared_with_other
        chart.description.isEnabled = false
        chart.setTouchEnabled(false)

        // scaling can now only be done on x- and y-axis separately
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)

        chart.axisLeft.setDrawLabels(false)
        chart.axisLeft.setDrawGridLines(false)
        chart.axisLeft.setDrawAxisLine(false)
        chart.axisLeft.axisMaximum = 100F
        chart.axisLeft.axisMinimum = 0F
        chart.axisRight.setDrawLabels(false)
        chart.axisRight.setDrawGridLines(false)
        chart.axisRight.setDrawAxisLine(false)
        chart.xAxis.setDrawLabels(false)
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.setDrawAxisLine(false)
        chart.legend.isEnabled = false

        val values = ArrayList<BarEntry>().apply {
            this.add(BarEntry(0F, others.toFloat()))
            this.add(BarEntry(1F, myPercentage.toFloat()))
        }

        val set1 = BarDataSet(values, "Data Set").apply {
            this.setColors(Color.rgb(44, 62, 80), Color.rgb(241, 196, 15))
            this.valueTextSize = 12F
            this.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float) = "${value.toInt()}%"
            }
            this.setDrawValues(true)
        }
        val dataSets = ArrayList<IBarDataSet>().apply {
            this.add(set1)
        }
        chart.data = BarData(dataSets).apply {
            barWidth = 0.9f
        }
        chart.setFitBars(true)
        chart.invalidate()
    }

    private fun getMyPhotos(list: List<ChallengeRecord>): List<String> {
        return list
                .sortedByDescending { it.id }
                .map { it.url ?: "" }
                .toList()
    }

    private fun getWeek(list: List<ChallengeRecord>, countPerWeeks: Int, startTime: Long): List<Int> {
        // challenge.time: 시작 시간.
        //
        val map = list.groupBy { classifyWeek(startTime, it.time ?: 0L) }

//        val max = map.keys.maxBy { it } ?: 4
        val max = 5
        val result = (0 until max).map { week ->
            if (map[week] == null) {
                0
            } else {
                (map[week]!!.size!!.toFloat() / countPerWeeks / 2 * 100).toInt()
            }
        }

        // TODO: 주차 변경하기.
//        val map = list.map {
//            Calendar.getInstance().apply { time = Date(it.time ?: 0L) }
//                    .get(Calendar.WEEK_OF_MONTH)
//        }.groupBy { it }
//
//        val max = map.keys.maxBy { it } ?: 4
//        val result = (0..max).map { week ->
//            if (map[week] == null) {
//                0
//            } else {
//                (map[week]!!.size!!.toFloat() / countPerWeeks / 2 * 100).toInt()
//            }
//        }

        return result
    }

    private fun getMyPercentage(list: List<ChallengeRecord>, willCount: Int): Int {
        val result = (list.size.toFloat() / willCount * 100).toInt()
        return result
    }

    private fun initPhotos(myPhotos: List<String>) {
        if (myPhotos.isEmpty()) {
            return
        }

        val adapter = PhotosAdapter(this, myPhotos, container_photos.columnWidth, container_photos.numColumns * 2)
        container_photos.adapter = adapter

        btn_more_photos.setOnClickListener { moveToDetailPhotos(myPhotos) }
    }

    private fun moveToDetailPhotos(photos: List<String>) {
        Intent(this@StatisticActivity, DetailPhotosActivity::class.java).apply {
            val arrayPhotos = ArrayList<String>().apply {
                this.addAll(photos)
            }
            this.putStringArrayListExtra("photos", arrayPhotos)
            this.putExtra("title", intent.getStringExtra("title"))
        }.also { startActivity(it) }
    }

    private fun classifyWeek(startTime: Long, time: Long): Int {
        val firstCheckPoint = startTime + 60480000
        val secondCheckPoint = firstCheckPoint + 60480000
        val thirdCheckPoint = secondCheckPoint + 60480000
        val fourthCheckPoint = thirdCheckPoint + 60480000

        return when {
            time < firstCheckPoint -> 0
            time < secondCheckPoint -> 1
            time < thirdCheckPoint -> 2
            time < fourthCheckPoint -> 3
            else -> 4
        }
    }
}
