package com.gachon.timechecker.statistic

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gachon.timechecker.R
import kotlinx.android.synthetic.main.activity_statistic.*

class DetailStatisticActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_statistic)

        val title = intent.getStringExtra("title")
        tv_title.text = title
    }
}
