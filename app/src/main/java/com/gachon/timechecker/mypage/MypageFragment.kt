package com.gachon.timechecker.mypage

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.gachon.timechecker.R
import com.gachon.timechecker.common.ExtensionFragment
import com.gachon.timechecker.common.FileCompress
import com.gachon.timechecker.common.FileManager
import com.gachon.timechecker.common.setImage
import com.gachon.timechecker.data.Challenge
import com.gachon.timechecker.repository.ChallengeRepository
import com.gachon.timechecker.repository.UserSetting
import com.gachon.timechecker.statistic.StatisticActivity
import kotlinx.android.synthetic.main.fragment_mypage.view.*
import java.io.File
import java.util.*

class MypageFragment : ExtensionFragment() {
    val TAG_CAMERA: Int = 10001
    val REQUEST_PERMISSIONS_PICTURES_RELATED = 20001

    private var profileIv: ImageView? = null
    private var mImageFile: File? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        startProgress()

        return inflater.inflate(R.layout.fragment_mypage, container, false).apply {
            bindView(this as ConstraintLayout)
        }
    }

    private fun bindView(layout: ConstraintLayout) {
        with(layout) {
            profileIv = findViewById<ImageView>(R.id.iv_profile)
            profileIv?.setImage(UserSetting.userPic)
            val tvName = findViewById<TextView>(R.id.tv_name)
            tvName.text = UserSetting.userName

            val editProfile = findViewById<Button>(R.id.btn_edit_image)
            editProfile.setOnClickListener {
                if (checkPermission()) {
                    onStartPictures()
                } else {
                    val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    ActivityCompat.requestPermissions(activity!!, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
                }
            }

            ChallengeRepository.getChallenges { challenges ->
                val map = challenges.groupBy {
                    Calendar.getInstance().apply {
                        this.time = Date(it.time)
                    }.get(Calendar.MONTH)
                }

                val viewList = arrayListOf<View>()
                val keys = map.keys.sortedByDescending { it }
                keys.forEach { month ->
                    var notDivideMade = true
                    map[month]?.forEach { challenge ->
                        ChallengeRepository.getCurrentChallengeStatus(challenge) {
                            val percent = if (challenge.willCount == 0) 100 else (it.size.toFloat() / challenge.willCount * 100).toInt()

                            if (notDivideMade) {
                                notDivideMade = false
                                getDivider(month)?.let {
                                    container_records.addView(it)
                                }
                            }

                            getChallengeView(challenge, percent)?.let {
                                val calendar = Calendar.getInstance().apply {
                                    this.time = Date(challenge.time)
                                }
                                if (calendar.get(Calendar.MONTH) == month) {
                                    container_records.addView(it)
                                }
                            }
                        }
                    }
                }

//                CoroutineScope(Dispatchers.IO).launch {
//                    delay(2000L)
//                    CoroutineScope(Dispatchers.Main).launch {
//                        viewList.forEach {
//
//                        }
//                    }
//                }

                endProgress()
            }
        }
    }

    private fun getChallengeView(challenge: Challenge, percentage: Int): View? {
        if (context == null) {
            return null
        }

        return ConstraintLayout.inflate(context, R.layout.item_challenge_record, null).apply {
            this.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        }.also { container ->
            val weeksList = challenge.weeks.split(", ")
            val title = when (weeksList.size){
                7 -> {
                    "토익 매일 공부하기 ($percentage%)"
                }
                else -> "토익 주 ${weeksList.size}회 공부하기 ($percentage%)"
            }
            container.findViewById<TextView>(R.id.tv_title).text = title
            container.setOnClickListener {
                Intent(activity, StatisticActivity::class.java).apply {
                    this.putExtra("title", title)
                    this.putExtra("weeks", challenge.weeks)
                    this.putExtra("startTime", challenge.time)
                    this.putExtra("willCount", challenge.willCount)
                    this.putExtra("challengeId", challenge.id)
                }.also {
                    startActivity(it)
                }
            }

            ConstraintSet().apply {
                clone(container as ConstraintLayout)
                setHorizontalBias(R.id.pointer, (percentage.toFloat()/100))
            }.also { it.applyTo(container as ConstraintLayout) }
        }
    }

    private fun getDivider(month: Int): View? {
        if (context == null) {
            return null
        }

        return ConstraintLayout.inflate(context, R.layout.item_challenge_record_divider, null).apply {
            this.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        }.also { it.findViewById<TextView>(R.id.tv_title).text = "${month}월" }
    }

    private fun onStartPictures() {
        val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(Intent.createChooser(cameraIntent, "Select Picture"), TAG_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            TAG_CAMERA -> {
                if (resultCode != Activity.RESULT_OK) {
                    return
                }

                val uri = data?.data
                if (uri != null) {
                    mImageFile = uriToImageFile(uri)
                    val fileName = "profile-" + System.currentTimeMillis() + ".png"
                    val compressed = FileCompress.getCompressedImageFile(mImageFile, activity)
                    FileManager.uploadFile(fileName, compressed!!, object : FileManager.UploadCallback {
                        override fun onSuccess(url: String) {
                            context?.let { Glide.with(it).load(mImageFile).into(profileIv!!) }
                            Toast.makeText(context, "이미지가 변경되었습니다.", Toast.LENGTH_SHORT).show()

                            UserSetting.userPic = url
                            UserSetting.saveProfileOnRemote()
                        }

                        override fun onFail() {
                            Toast.makeText(context, "이미지 업로드에 실패하였습니다.", Toast.LENGTH_SHORT).show()
                        }
                    })
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        var allGranted = true
        val cnt = permissions.size
        for (i in 0 until cnt) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
            } else {
                allGranted = false
                Toast.makeText(context, "'사진'을 이용하기 위해서는 접근 권한을 허용해주셔야 합니다.", Toast.LENGTH_SHORT).show()
                break
            }
        }

        if (allGranted) {
            when (requestCode) {
                REQUEST_PERMISSIONS_PICTURES_RELATED -> onStartPictures()
            }
        }
    }

    private fun checkPermission(): Boolean {
        if (context == null) return false

        val permissionsArr = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity!!, permissionsArr, REQUEST_PERMISSIONS_PICTURES_RELATED)
                false
            } else {
                true
            }
        }
        return true
    }

    private fun uriToImageFile(uri: Uri): File? {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activity?.contentResolver?.query(uri, filePathColumn, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val filePath = cursor.getString(columnIndex)
                cursor.close()
                return File(filePath)
            }
            cursor.close()
        }
        return null
    }

    companion object {
        @JvmStatic
        fun newInstance() = MypageFragment()
    }
}
