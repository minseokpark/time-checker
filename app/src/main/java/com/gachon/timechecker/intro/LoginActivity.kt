package com.gachon.timechecker.intro

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.gachon.timechecker.MainActivity
import com.gachon.timechecker.R
import com.gachon.timechecker.repository.UserSetting
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    val RC_SIGN_IN = 1000

    // Build a GoogleSignInClient with the options specified by gso.
    lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (UserSetting.isLogin) {
            moveToMain()
            return
        }

        setContentView(R.layout.activity_login)

        prepareWithGoogle()
    }

    private fun prepareWithGoogle() {
        // Configure Google Sign In
        // TODO Must Change Token.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("269145117923-co52stbdsdk722mp13mm6no2923putec.apps.googleusercontent.com")
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        sign_in_button.setSize(SignInButton.SIZE_WIDE);
        sign_in_button.setOnClickListener {
            signIn()
        }
    }

    private fun moveToMain() {
        Intent(this, MainActivity::class.java).apply {
            this.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }.also { startActivity(it) }
        finish()
    }

    private fun updateUI(account: GoogleSignInAccount?) {
        account?.let {
            with(UserSetting) {
                this.userName = it.displayName!!
                this.userEmail = it.email!!
                this.userPic = it.photoUrl.toString()
                this.userId = it.id!!

                this.isLogin = true
            }
        } ?: let { return }

        UserSetting.checkNewSignUp(account.id!!) { isExist ->
            if (isExist) {
                moveToMain()
                return@checkNewSignUp
            }

            UserSetting.saveProfileOnRemote()
            moveToMain()
        }
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
            Toast.makeText(this, R.string.txt_fail_to_login, Toast.LENGTH_SHORT).show()
        }

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
}
