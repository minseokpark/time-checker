package com.gachon.timechecker;

/**
 * Created by minseok on 2019-09-23.
 * TimeChecker.
 */
public class GlobalDef {
    public final static String USER_ID = "userId";
    public final static String USER_NAME = "userName";
    public final static String USER_EMAIL = "userEmail";
    public final static String USER_PIC = "userPic";

    public final static String POST_ID = "id";
    public final static String POST_SUBJECT = "subject";
    public final static String POST_CONTENT = "content";
    public final static String POST_AUTHOR = "author";
    public final static String POST_AUTHOR_IMAGE = "authorPhoto";
    public final static String POST_PHOTO = "photo";

    public final static String COMMENT_ID = "id";
    public final static String COMMENT_POST_ID = "postId";
    public final static String COMMENT_AUTHOR = "userName";
    public final static String COMMENT_AUTHOR_PHOTO = "photo";
    public final static String COMMENT_CONTENT = "content";

    public final static String CHALLENGE_ID = "id";
    public final static String CHALLENGE_USER = "user";
    public final static String CHALLENGE_WEEKS = "weeks";
    public final static String CHALLENGE_START_TIME = "startTime";
    public final static String CHALLENGE_END_TIME = "endTime";
    public final static String CHALLENGE_RECORDED_START_TIME = "recordedStartTime";
    public final static String CHALLENGE_RECORDED_END_TIME = "recordedEndTime";
    public final static String CHALLENGE_WILL_COUNT = "willCount";
    public final static String CHALLENGE_IS_DONE = "isDone";
    public final static String CHALLENGE_CURRENT_TIME= "time";

    public final static String CHALLENGE_RECORD_ID = "id";
    public final static String CHALLENGE_RECORD_RECORDED_TYPE = "type";
    public final static String CHALLENGE_RECORD_RECORDED_TIME = "time";
    public final static String CHALLENGE_RECORD_CHALLENGE_ID = "challengeId";
    public final static String CHALLENGE_RECORD_IMAGE_URL= "url";

    public final static String FIRESTORE_USERS = "users";
    public final static String FIRESTORE_POSTS = "posts";
    public final static String FIRESTORE_COMMENTS = "comments";
    public final static String FIRESTORE_CHALLENGE = "challenge";
    public final static String FIRESTORE_CHALLENGE_RECORD = "challengeRecord";

    public final static String URL_FIRESTORE_STORAGE= "gs://time-checker-190f6.appspot.com";

    public final static String URL_TOEIC = "https://www.amideast.org/sites/default/files//toeic%20resized_4.png";
}