package com.gachon.timechecker.repository

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.gachon.timechecker.GlobalDef
import com.google.firebase.firestore.FirebaseFirestore

/**
 * Created by minseok on 2019-09-23.
 * TimeChecker.
 */
object UserSetting {
    private val TAG = "usersetting"
    private val USER_ID = "user_id"
    private val USER_NAME = "user_name"
    private val USER_PIC = "user_pic"
    private val USER_EMAIL = "user_email"
    private val USER_IS_LOGIN = "user_is_login"

    lateinit var mSharedPreferences: SharedPreferences

    var userId: String
        get() = getString(USER_ID)
        set(value) = setWith(USER_ID, value)

    var userName: String
        get() = getString(USER_NAME)
        set(value) = setWith(USER_NAME, value)

    var userPic: String
        get() = getString(USER_PIC)
        set(value) = setWith(USER_PIC, value)

    var userEmail: String
        get() = getString(USER_EMAIL)
        set(value) = setWith(USER_EMAIL, value)

    var isLogin: Boolean
        get() = getBoolean(USER_IS_LOGIN)
        set(value) = setWith(USER_IS_LOGIN, value)

    private fun getInt(name: String) = mSharedPreferences.getInt(name, 0)
    private fun getBoolean(name: String) = mSharedPreferences.getBoolean(name, false)
    private fun getString(name: String) = mSharedPreferences.getString(name, "")
    private fun setWith(name: String, value: Any) {
        val edit = mSharedPreferences.edit()
        when (value) {
            is Int      -> edit.putInt(name, value)
            is String   -> edit.putString(name, value)
            is Float    -> edit.putFloat(name, value)
            is Boolean  -> edit.putBoolean(name, value)
        }

        edit.commit()
    }

    fun saveProfileOnRemote() {
        Log.d(TAG, "saving data on server...")

        val db = FirebaseFirestore.getInstance()

        val user = HashMap<String, Any>().apply {
            this[GlobalDef.USER_ID] = userId
            this[GlobalDef.USER_NAME] = userName
            this[GlobalDef.USER_EMAIL] = userEmail
            this[GlobalDef.USER_PIC] = userPic
        }

        db.collection(GlobalDef.FIRESTORE_USERS)
                .document(userId)
                .get()
                .addOnSuccessListener {
                    if (!it.exists()) {
                        db.collection(GlobalDef.FIRESTORE_USERS)
                                .document(userId)
                                .set(user)
                                .addOnSuccessListener {
                                    Log.w(TAG, "유저정보 저장")
                                }
                    } else {
                        db.collection(GlobalDef.FIRESTORE_USERS)
                                .document(userId)
                                .update(user)
                                .addOnSuccessListener {
                                    Log.w(TAG, "유저정보 업데이트")
                                }
                    }
                }
    }

    fun checkNewSignUp(id: String, callback: (Boolean) -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_USERS)
                .whereEqualTo(GlobalDef.USER_ID, id)
                .get()
                .addOnSuccessListener {
                    callback(it.documents.size != 0)
                }.addOnFailureListener {
                    callback(false)
                }
    }

    fun init(context: Context) {
        mSharedPreferences = context.getSharedPreferences("timechecker_preferences", Activity.MODE_PRIVATE)
    }

    fun clear() {
        with(mSharedPreferences.edit()) {
            clear()
            commit()
        }
    }

    fun loadProfile(id: String, callback: () -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_USERS)
                .whereEqualTo(GlobalDef.USER_ID, id)
                .get()
                .addOnSuccessListener {
                    if (it.documents.size == 0) return@addOnSuccessListener

                    with(it.documents.last()) {
                        userId = get(GlobalDef.USER_ID) as String
                        userName = get(GlobalDef.USER_NAME) as String
                        userEmail = get(GlobalDef.USER_EMAIL) as String
                        userPic = get(GlobalDef.USER_PIC) as String
                    }

                    callback()
                }
    }
}