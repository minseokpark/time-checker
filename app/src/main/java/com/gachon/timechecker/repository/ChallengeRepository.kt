package com.gachon.timechecker.repository

import android.util.Log
import com.gachon.timechecker.GlobalDef
import com.gachon.timechecker.data.Challenge
import com.gachon.timechecker.data.ChallengeRecord
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

/**
 * Created by minseok on 2019-09-24.
 * TimeChecker.
 */
object ChallengeRepository {
    val TAG = "ChallengeRepository"
    val db = FirebaseFirestore.getInstance()
    fun requestChallenge(weekday: String, startTime: String, endTime: String, success: () -> Unit, failure: (String) -> Unit, finish: () -> Unit) {
        val db = FirebaseFirestore.getInstance()
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE)

        val weeks = weekday.split(", ")
        val willCount = when (weeks.size) {
            2 -> 16
            3 -> 24
            5 -> 40
            7 -> 60
            else -> 0
        }

        getAllChallenges {
            if (it.any { it.user == UserSetting.userId && !it.isDone }) {
                failure("진행 중인 챌린지가 존재합니다.")
                finish()
                return@getAllChallenges
            }

            val index = it.size
            val challenge = Challenge(
                    id = index,
                    user = UserSetting.userId,
                    weeks = weekday,
                    startTime = startTime,
                    endTime = endTime,
                    time = Date().time,
                    willCount = willCount
            )
            challengeRef
                    .add(challenge)
                    .addOnSuccessListener { success() }
                    .addOnCompleteListener { finish() }
        }
    }

    fun getChallenges(callback: (List<Challenge>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE)
        challengeRef
                .whereEqualTo(GlobalDef.CHALLENGE_USER, UserSetting.userId)
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(Challenge::class.java) }.toList().also { challenges ->
                        callback(challenges)
                    }
                }
    }

    fun getCurrentChallengeStatus(challenge: Challenge, callback: (List<ChallengeRecord>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE_RECORD)
        challengeRef
                .whereEqualTo(GlobalDef.CHALLENGE_RECORD_CHALLENGE_ID, challenge.id)
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(ChallengeRecord::class.java) }.toList().also { challenges ->
                        callback(challenges)
                    }
                }
    }

    fun recordCheckChallenge(challenge: Challenge, time: Long, type: String, url: String?, callback: ()->Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE_RECORD)
        challengeRef
                .get()
                .addOnSuccessListener {
                    val index = it.size()
                    challengeRef
                            .add(ChallengeRecord(index, type, time, challenge.id, url))
                            .addOnSuccessListener { callback() }
                }
    }

    fun getChallengeRecords(challengeId: Int, callback: (List<ChallengeRecord>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE_RECORD)
        challengeRef
                .whereEqualTo(GlobalDef.CHALLENGE_RECORD_CHALLENGE_ID, challengeId)
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(ChallengeRecord::class.java) }.toList().also { records ->
                        callback(records)
                    }
                }
    }

    fun getChallengeTotalPercentage(callback: (List<Challenge>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE)
        challengeRef
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(Challenge::class.java) }.toList().also { challenges ->
                        callback(challenges)
                    }
                }
    }

    fun getChallengeRecordTotalPercentage(callback: (List<ChallengeRecord>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE_RECORD)
        challengeRef
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(ChallengeRecord::class.java) }.toList().also { records ->
                        callback(records)
                    }
                }
    }

    fun checkIfStart(challenge: Challenge, callback: (Status) -> Unit) {
        db.collection(GlobalDef.FIRESTORE_CHALLENGE_RECORD)
                .whereEqualTo(GlobalDef.CHALLENGE_RECORD_CHALLENGE_ID, challenge.id)
                .get()
                .addOnSuccessListener successListener@{
                    val last = it.documents.maxBy { it[GlobalDef.CHALLENGE_RECORD_RECORDED_TIME] as Long }
                            ?.toObject(ChallengeRecord::class.java)

                    // null일 경우는 기록이 없는 것이므로 Start
                    Log.d(TAG, last.toString())

                    if (last == null) {
                        callback(Status.Start())
                        return@successListener
                    }

                    val current = Calendar.getInstance()
                    val lastTime = Calendar.getInstance().apply {
                        this.time = Date(last.time!!)
                    }

                    val status = when {
                        current.get(Calendar.YEAR) != lastTime.get(Calendar.YEAR) ||
                                current.get(Calendar.MONTH) != lastTime.get(Calendar.MONTH) ||
                                current.get(Calendar.DAY_OF_MONTH) != lastTime.get(Calendar.DAY_OF_MONTH)
                        -> Status.Start()

                        last.type == "end"
                        -> Status.Done()

                        else -> {
                            // 남은 시간 = 끝나는 시각 - 현재시각 (이걸로)
                            // 남은 시간 = (시작한 시각 + 공부해야하는 시각) - 현재시각
                            val timeItem = challenge.endTime.split(":")
                            val hour = timeItem[0].toInt()
                            val min = timeItem[1].toInt()

                            val currentTime = current.timeInMillis
                            val endTime = Calendar.getInstance().apply {
                                set(Calendar.HOUR_OF_DAY, hour)
                                set(Calendar.MINUTE, min)
                            }.timeInMillis

                            val leftTime = endTime - currentTime
                            Status.End(leftTime)
                        }
                    }

                    callback(status)
                }
    }

    sealed class Status {
        class Start : Status()
        class End(val leftTime: Long) : Status()
        class Done : Status()
    }

    fun getAllChallenges(callback: (List<Challenge>) -> Unit) {
        val challengeRef = db.collection(GlobalDef.FIRESTORE_CHALLENGE)
        challengeRef
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull { it.toObject(Challenge::class.java) }.toList().also { challenges ->
                        callback(challenges)
                    }
                }
    }
}