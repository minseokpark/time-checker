package com.gachon.timechecker.repository

import android.util.Log
import com.gachon.timechecker.GlobalDef
import com.gachon.timechecker.data.Comment
import com.gachon.timechecker.data.Post
import com.google.firebase.firestore.FirebaseFirestore

/**
 * Created by minseok on 2019-09-24.
 * TimeChecker.
 */
object PostRepository {
    val TAG = "PostRepository"
    /**
     * 게시글 쓰기
     */
    fun addPost(subject: String, content: String, photo: String?, callback: () -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_POSTS)
                .get().addOnSuccessListener {
                    val index = it.size()

                    val data = Post(index, subject, content, UserSetting.userName, UserSetting.userPic, photo)
                    db.collection(GlobalDef.FIRESTORE_POSTS)
                            .add(data)
                            .addOnSuccessListener {
                                Log.d(TAG, "Success to save data ...")
                                callback()
                            }
                }
    }

    /**
     * 모든 게시글 리스트 가져오기
     */
    fun getAllPosts(callback: (List<Post>) -> Unit) {
        Log.d(TAG, "fetch data from server...")

        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_POSTS)
                .get()
                .addOnSuccessListener {
                    val posts = it.documents.mapNotNull { document ->
                        document.toObject(Post::class.java)
                    }.toList().sortedByDescending { post -> post.id }

                    callback(posts)
                }
    }

    /**
     * 특정 게시글 가져오기
     */
    fun getPostInfo(postNo: Int, callback: (Post) -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_POSTS)
                .whereEqualTo(GlobalDef.POST_ID, postNo)
                .get()
                .addOnSuccessListener {
                    if (it.documents.isEmpty()) {
                        return@addOnSuccessListener
                    }

                    it.documents.last().toObject(Post::class.java)?.let { post ->
                        callback(post)
                    }
                }
    }

    /**
     * 댓글 쓰기
     */
    fun addComment(postNo: Int, content: String, callback: (Comment) -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_COMMENTS)
                .whereEqualTo(GlobalDef.COMMENT_POST_ID, postNo)
                .get()
                .addOnSuccessListener {
                    val index = it.size()

                    val data = Comment(index, postNo, UserSetting.userName, UserSetting.userPic, content)
                    db.collection(GlobalDef.FIRESTORE_COMMENTS)
                            .add(data)
                            .addOnSuccessListener {
                                Log.d(TAG, "Success to save data ...")
                                callback(data)
                            }
                }
    }

    /**
     * 댓글 가져오기
     */
    fun getComments(postNo: Int, comments: (List<Comment>) -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection(GlobalDef.FIRESTORE_COMMENTS)
                .whereEqualTo(GlobalDef.COMMENT_POST_ID, postNo)
                .get()
                .addOnSuccessListener {
                    it.documents.mapNotNull {
                        it.toObject(Comment::class.java)
                    }.also { list ->
                        list.sortedByDescending { it.id }
                        comments(list) }
                }
    }
}